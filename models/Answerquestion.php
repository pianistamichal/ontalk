<?php
/**
 * Created by PhpStorm.
 * User: Phate-Admin
 * Date: 2015-09-12
 * Time: 17:35
 */

namespace app\models;

use Yii;
use yii\base\Model;

class AnswerQuestion extends Model
{
    public $message_body;
    public function rules()
    {
        return [
            [['message_body'], 'required'],
        ];
    }
    public function sendMail($email, $subject){
            $message = Yii::$app->mailer->compose();

            $message->setFrom('ontalk.contact@gmail.com')
            ->setTo($email)
            ->setSubject("Re: ".$subject)
            ->setTextBody($this->message_body)
            ->send();
    }

}