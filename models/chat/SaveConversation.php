<?php
namespace app\models\chat;

use app\models\activeRecord\Conversation;
use app\models\activeRecord\Users;
use Yii;
use yii\base\Model;

class SaveConversation extends Model {
    public function deleteOldConversation() {
        $myModel = Users::findOne(['username' => Yii::$app->user->identity->username]);
        $model = Conversation::find()->where(['username1' => $myModel->username])->asArray()->all();

        if(sizeof($model) >= 10) {
            $modelToDelete = Conversation::findOne(['username1' => $myModel->username]);
            return $modelToDelete->delete();
        }

        return false;
    }
    public function saveConversation() {
        $myModel = Users::findOne(['username' => Yii::$app->user->identity->username]);

        $result = new Conversation();
        $result->username1 = $myModel->username;
        $result->text = $_POST['data'];
        $result->save();
    }
}