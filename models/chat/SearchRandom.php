<?php
/**
 * Created by PhpStorm.
 * User: PianistaMichal
 * Date: 16.12.2015
 * Time: 13:31
 */

namespace app\models\chat;

use app\models\activeRecord\UsersWaiting;

class SearchRandom extends Search {
    public function __construct() {
        parent::__construct();
    }

    protected function addUserToWaiting($room = null, $topic = null)
    {
        $this->searcher = new UsersWaiting();
        $this->searcher->username = $this->username;
        $type = array('type' => 0);
        $this->searcher->chat_type_waiting = json_encode($type);
        $this->searcher->room = $room;
        $this->searcher->topic = $topic;

        $this->searcher->save();
    }

    public function assignWaitingPartner() {
        $type = array('type' => 0);
        $type = json_encode($type);
        $this->partner = UsersWaiting::find()->where(['type' => $type])->where(['not in', 'username', $this->username])->one();
    }

    public function assignFoundPartner() {
        $this->partner = UsersWaiting::find()->where(['room' => $this->room])->where(['not in', 'username', $this->username])->one();
    }
}