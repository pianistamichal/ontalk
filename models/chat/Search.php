<?php
/**
 * Created by PhpStorm.
 * User: PianistaMichal
 * Date: 16.12.2015
 * Time: 13:36
 */

namespace app\models\chat;

use app\models\activeRecord\Topics;
use app\models\activeRecord\UsersWaiting;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\base\Security;
use yii\web\Session;

abstract class Search extends Model
{
    protected $username = null;
    protected $searcher = null;
    protected $partner = null;
    protected $room = null;
    protected $topic = null;
    protected $isConnected = false;

    public function __construct() {
        parent::__construct();
        $this->username = $this->returnQuestOrUser();
    }

    public function waitingChatterAction() {
        $this->assignSearcher();
        if($this->searcher->room != null) {
            $this->room = $this->searcher->room;
            $this->topic = $this->searcher->topic;
            $this->assignFoundPartner();
            $this->saveToSession();
            $this->deletePartnerFromWaiting();
            $this->deleteSearcherFromWaiting();
            $this->isConnected = true;
        }
    }

    public function newChatterAction() {
        $this->assignWaitingPartner();
        if($this->isPartnerFound()) {
            $this->room = $this->createRoom();
            $this->topic = $this->createTopic();
            $this->partner->room = $this->room;
            $this->partner->topic = $this->topic;
            $this->partner->save();
            $this->saveToSession();

            $this->isConnected = true;
        }

        $this->addUserToWaiting($this->room, $this->topic);
    }
    /**
     * @return string|null
     */
    private function returnQuestOrUser() {
        try {
            if(Yii::$app->user->isGuest) {
                if(isset($_COOKIE['PHPSESSID'])) {
                    return $_COOKIE['PHPSESSID'];
                }
            } else {
                return Yii::$app->user->identity->username;
            }
        } catch(Exception $e) {
            return null;
        }

        return null;
    }

    private function createRoom() {
        $randString = new Security();
        return $randString->generateRandomString();
    }

    private function createTopic() {
        return Topics::randomTopic();
    }

    public function saveToSession() {
        $session = new Session();
        $session->open();
        $session['room'] = $this->room;
        $session['topic'] = $this->topic;
    }

    /**
     * @return bool
     */
    public function isUserWaiting() {
        return (UsersWaiting::find()->where(['username' => $this->username])->one() != null) ? true : false;
    }

    public function assignSearcher() {
        $this->searcher = UsersWaiting::find()->where(['username' => $this->username])->one();
    }

    public function getIsConnected() {
        return $this->isConnected;
    }

    public function deleteSearcherFromWaiting() {
        $this->assignSearcher();

        if($this->searcher != null) {
            $this->searcher->delete();
        } else {
            1==0;
        }
    }

    public function deletePartnerFromWaiting() {
        $this->assignFoundPartner();

        if($this->partner != null) {
            $this->partner->delete();
        } else {
            1==0;
        }
    }

    public function isPartnerFound() {
        return $this->partner != null ? true : false;
    }

    public function getTopic() {
        return $this->topic;
    }

    public function getRoom() {
        return $this->room;
    }

    //adds user to database as waiting
    protected abstract function addUserToWaiting($room = null, $topic = null);

    //assign $partner if found partner
    public abstract function assignWaitingPartner();

    //assign $partner if found partner
    public abstract function assignFoundPartner();
}