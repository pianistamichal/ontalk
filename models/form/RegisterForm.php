<?php

namespace app\models\form;

use app\models\activeRecord\Users;
use Yii;
use yii\base\Model;
use yii\base\Security;

class RegisterForm extends Model{
    public $login;
    public $password;
    public $password_repeat;
    public $email;
    public $captcha ;

    public function rules(){
        return [
                [['login','email','password','password_repeat','captcha'],'required'],

                ['password','string','length'=>[5,40]],
                ['password_repeat','compare','compareAttribute'=>'password','message'=>"Passwords must be the same"],

                ['login','string','length' => [4,25]],
                ['login','unique','targetClass' => 'app\models\activeRecord\Users','targetAttribute' => 'username'],

                [['captcha'],'captcha', 'captchaAction' => 'site/captcha', 'caseSensitive' => false],

                ['email','unique','targetClass' => 'app\models\activeRecord\Users','targetAttribute' => 'email'],
                ['email','email']
                ];
    }
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'username'),
            'password'=>  Yii::t('app', 'password'),
            'password_repeat'=>Yii::t('app', 'repeat password'),
            'email' => Yii::t('app', 'email'),
            'captcha'=>  Yii::t('app', 'Captcha'),

        ];
    }
    public function register()
    {
        $password_hash = new Security();

        $user = new Users();
        $user->email = $this->email;
        $user->username = $this->login;

        //HASHING PASSWORD WITH SALT
        $user->password = $password_hash->generatePasswordHash($this->password);
        $user->save();
        return $user;
    }







}

