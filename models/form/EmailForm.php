<?php

namespace app\models\form;

use app\models\activeRecord\Users;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class EmailForm extends Model
{
    public  $password;
    public  $email;
    public  $email_repeat;

    public function rules()
    {
        return [
            // username and password are both required
            [['password', 'email','email_repeat'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

            ['email_repeat','compare','compareAttribute'=>'email','message'=>"Email address must be the same!"],
            ['email','unique','targetClass' => 'app\models\activeRecord\Users','targetAttribute' => 'email'], /*check if there is users with the same name*/
            ['email','email']
        ];
    }
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'password'),
            'email' => Yii::t('app', 'email'),
            'email_repeat' => Yii::t('app', 'repeat email'),
        ];
    }
    public function validatePassword($attribute, $params)
    {

      //  $user = Yii::$app->session->get('LoggedUser');
        $user = Users::findByUsername(Yii::$app->user->identity->username);
        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError($attribute, 'Incorrect username or password.');
        }
    }
    public function setNewEmail(){
    //    $user =  Yii::$app->session->get('LoggedUser');
        $user = Users::findByUsername(Yii::$app->user->identity->username);
        $user->email = $this->email;
        $user->save();
    }

}