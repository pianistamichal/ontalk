<?php

namespace app\models\form;

use app\models\activeRecord\Users;
use Yii;
use yii\base\Model;
use yii\base\Security;

/**
 * LoginForm is the model behind the login form.
 */
class PasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $new_password_repeat;

    public function rules()
    {
        return [
            [['old_password', 'new_password','new_password_repeat'], 'required'],

            ['old_password', 'validatePassword'],

            ['new_password','string','length'=>[5,40]],
            ['new_password_repeat','compare','compareAttribute'=>'new_password','message'=>Yii::t('app','Password must be the same')],
        ];
    }
    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('app', 'old password'),
            'new_password' => Yii::t('app', 'new password'),
            'new_password_repeat' => Yii::t('app', 'repeat new password'),
        ];
    }
    public function validatePassword($attribute, $params)
    {
        $user = Users::findByUsername(Yii::$app->user->identity->username);
            if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError($attribute, Yii::t('app','Incorrect password'));
            }
    }
    public function SetNewPassword(){
        $password_hash = new Security();
        $user = Users::findByUsername(Yii::$app->user->identity->username);
        $user->password = $password_hash->generatePasswordHash($this->new_password);
        $user->save();
    }

}