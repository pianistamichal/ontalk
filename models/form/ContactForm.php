<?php

namespace app\models\form;

use app\models\activeRecord\Contact;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    public function rules()
    {
        return [
            // email, subject and body are required
            [['email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'verifyCode' => Yii::t('app', 'Verification Code'),
            'email'=>Yii::t('app', 'email'),
            'subject'=>Yii::t('app', 'subject'),
            'body'=>Yii::t('app', 'body'),
            'contact-button'=> "test",
        ];
    }

    public function  saveIncomingMessage(){
        $ctc = new Contact();

        $ctc->email = $this->email;
        $ctc->subject = $this->subject;
        $ctc->body = $this->body;
        $ctc->save();
    }
}
