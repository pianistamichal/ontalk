<?php


namespace app\models\form;

use Yii;
use app\models\activeRecord\Topics;
use yii\base\Model;

class AdminPanelForm extends  Model
{
    public $topic;
    public  function rules(){
        return [
            [ ['topic'],'required']
        ];
    }
    public function attributeLabels()
    {
        return [
            'topic' => Yii::t('app', 'Topic'),
        ];
    }
    public function addNewTopic(){
        $NewOne = new Topics();
        $NewOne->topic = $this->topic;
        $NewOne->save();
    }
    public function removeTopic(){
        $topic = new Topics();
        $topicToRemove = $topic->find()->where(['topic'=>$this->topic])->one();
        if($topicToRemove){
            $topicToRemove->delete();
            return true;
        }
        else{
            return false;
        }
    }
}