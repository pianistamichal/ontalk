<?php
/**
 * Created by PhpStorm.
 * User: Phate-Admin
 * Date: 2015-08-22
 * Time: 18:50
 */

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\activeRecord\Users;

class DeleteAccountForm extends Model
{
    public $password;


    public function rules()
    {

        return [
            [['password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'password'),
        ];
    }
    public function validatePassword($attribute, $params)
    {

        $user = Users::findByUsername(Yii::$app->user->identity->username);
        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError($attribute, 'Incorrect password.');
        }

    }
    public function deleteAccount()
    {
        if(Yii::$app->user->identity->user_role==1){
            Yii::$app->session->setFlash('Admin_Remove', 'Admin are you kiddin me??');
            return false;
        }
        else {
            $user = Users::findByUsername(Yii::$app->user->identity->username);
            $user->delete();
            return true;
        }
    }

}