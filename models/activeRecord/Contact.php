<?php
/**
 * Created by PhpStorm.
 * User: Phate-Admin
 * Date: 2015-08-27
 * Time: 16:40
 */

namespace app\models\activeRecord;

use yii\db\ActiveRecord;

class Contact extends ActiveRecord
{
    public static function tableName()
    {
        return 'contact';
    }
}