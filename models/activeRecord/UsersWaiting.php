<?php
/**
 * Created by PhpStorm.
 * User: PianistaMichal
 * Date: 16.12.2015
 * Time: 13:40
 */

namespace app\models\activeRecord;


use yii\db\ActiveRecord;

class UsersWaiting extends ActiveRecord
{
    public static function tableName()
    {
        return 'users_waiting';
    }
}