<?php

namespace app\models\activeRecord;

use yii\db\ActiveRecord;
use Yii;

class Conversation extends ActiveRecord
{
    public static function tableName()
    {
        return 'conversation';
    }

    //function changes JSON string to dispayable text for site/messages
    public function craftTextToDisplay($text){
        $numberOfCharacters = 180;
        $sentence = "";
        $dbTextJSON = json_decode($text, true);

        foreach($dbTextJSON as $current ){
            if(key($current)=="me"){
                $temp = $sentence;
                $sentence = $temp . Yii::t("app","Me:" ). $current["me"]. " " ;
            }
            else{
                $temp = $sentence;
                $sentence = $temp . Yii::t("app","Friend:" ) . $current["friend"]. " " ;
            }
        }
        $cut ="...".substr($sentence, -$numberOfCharacters);
        return $cut;
    }

    private function wrapSentenceWithHtml($text , $WhoIsTalking){
        if ( $WhoIsTalking == "M") {
            return  "<div class=\"Area\"><blockquote class=\"example-obtuse L\">" . $text . "</blockquote></div>";
        } else if($WhoIsTalking == "F"){
            return  "<div class=\"Area\"><blockquote class=\"example-obtuse R\">" . $text . "</blockquote></div>";
        }
        else{
            throw new \yii\base\Exception("wrong function input");
        }
    }

    public function convertTextToDisplay($inputText)
    {
        $sentence = "";
        $dbTextJSON = json_decode($inputText, true);

        foreach($dbTextJSON as $current ){
            if(key($current)=="me"){
                $temp = $sentence;
                $sentence = $temp . $this->wrapSentenceWithHtml($current["me"],"M")  ;
            }
            else{
                $temp = $sentence;
                $sentence = $temp . $this->wrapSentenceWithHtml($current["friend"],"F")  ;
            }
        }
        return $sentence;
    }
}