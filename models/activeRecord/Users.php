<?php

namespace app\models\activeRecord;

use Yii;
use yii\base\Security;
use yii\db\ActiveRecord;
use \yii\web\IdentityInterface;

class Users extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }
    public function setNotification($n) {
        Yii::$app->session['user_' .$this->id . '_notification'] = $n;
    }

    public function getNotification() {
        $n = Yii::$app->session['user_' .$this->id . '_notification'];
        Yii::$app->session['user_' .$this->id . '_notification'] = NULL;
        return $n;
    }
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->ID;
    }
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
        //return static::find()->where(['username' => $username])->one();
    }
    public static function findById($id)
    {
        return static::find()->where(['ID' => $id])->one();
    }

    public function getAuthKey()
    {
       // return $this->authKey;
        return "test1";
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    public function validatePassword($password)
    {
        $sec = new Security();
        return $sec->validatePassword($password, $this->password);
    }
}

