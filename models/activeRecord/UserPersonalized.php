<?php
namespace app\models\activeRecord;

use yii\db\ActiveRecord;

class UserPersonalized extends ActiveRecord {
    public static function tableName()
    {
        return 'user_personalized';
    }
}