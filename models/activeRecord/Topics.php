<?php

namespace app\models\activeRecord;


use yii\db\ActiveRecord;
use yii\gii\generators;

class Topics extends ActiveRecord
{
    public static function tableName()
    {
        return 'topics';
    }

    public static function randomTopic()
    {
        $maxID = self::find()->select("ID")->max("ID"); //last ID number
        $minID = self::find()->select("ID")->min("ID"); //minimum ID
        //if database is empty
        if($maxID == null)
            return "";

        $RandRecordNumber = rand($minID,$maxID);
        $topics = self::find()->where(['ID' => $RandRecordNumber])->one();
        $count = 1;
        while($topics == null){//taking next ID
            $topics = self::find()->where(['ID' => $RandRecordNumber+ $count])->one();
            $count++;
        }
        return $topics->topic;
    }
}