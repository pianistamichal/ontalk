<?php return array (
  'lifetime' => 1456027390,
  'data' => 
  array (
    'PluginDBStatsMetadata' => 
    array (
      'description' => 'DBStats_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExampleCommandMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to create a console command.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleCommand',
    ),
    'PluginExampleReportMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to define and display a data report.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleReport',
    ),
    'PluginExampleSettingsPluginMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to define and how to access plugin settings.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleSettingsPlugin',
    ),
    'PluginExampleThemeMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: example of how to create a simple Theme.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => true,
      'require' => 
      array (
      ),
      'name' => 'ExampleTheme',
      'stylesheet' => 'stylesheets/theme.less',
    ),
    'PluginExampleTrackerMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to track additional custom data creating new database table columns.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleTracker',
    ),
    'PluginExampleUIMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to display data tables, graphs, and the UI framework.',
      'homepage' => 'http://piwik.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'email' => 'hello@piwik.org',
          'homepage' => 'http://piwik.org',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '1.0.1',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleUI',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'framework',
        2 => 'platform',
        3 => 'ui',
        4 => 'visualization',
      ),
    ),
    'PluginExampleVisualizationMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to create a new custom data visualization.',
      'homepage' => 'http://piwik.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'The Piwik Team',
          'email' => 'hello@piwik.org',
          'homepage' => 'http://piwik.org',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleVisualization',
      'keywords' => 
      array (
        0 => 'SimpleTable',
      ),
    ),
    'PluginMobileAppMeasurableMetadata' => 
    array (
      'description' => 'Analytics for Mobile: lets you measure and analyze Mobile Apps with an optimized perspective of your mobile data.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'MobileAppMeasurable',
    ),
  ),
);