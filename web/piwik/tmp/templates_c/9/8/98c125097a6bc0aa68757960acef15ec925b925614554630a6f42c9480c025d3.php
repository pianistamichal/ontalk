<?php

/* @CorePluginsAdmin/safemode.twig */
class __TwigTemplate_98c125097a6bc0aa68757960acef15ec925b925614554630a6f42c9480c025d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <meta name=\"robots\" content=\"noindex,nofollow\">
        <style type=\"text/css\">
            html, body {
                background-color: white;
            }
            td {
                border: 1px solid #ccc;
                border-collapse: collapse;
                padding: 5px;
            }
            table {
                border-collapse: collapse;
                border: 0px;
            }
            a {
                text-decoration: none;
            }
            a:hover {
                text-decoration: underline;
            }
        </style>
    </head>
    <body>

        <h1>A fatal error occurred</h1>

        <div style=\"width: 640px\">

        ";
        // line 31
        if ( !(isset($context["isAnonymousUser"]) ? $context["isAnonymousUser"] : $this->getContext($context, "isAnonymousUser"))) {
            // line 32
            echo "            <p>
                The following error just broke Piwik";
            // line 33
            if ((isset($context["showVersion"]) ? $context["showVersion"] : $this->getContext($context, "showVersion"))) {
                echo " (v";
                echo twig_escape_filter($this->env, (isset($context["piwikVersion"]) ? $context["piwikVersion"] : $this->getContext($context, "piwikVersion")), "html", null, true);
                echo ")";
            }
            echo ":
                <pre>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "message", array()), "html", null, true);
            echo "</pre>
                in
                <pre>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "file", array()), "html", null, true);
            echo " line ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "line", array()), "html", null, true);
            echo "</pre>
            </p>

            <hr>
            <h3>Troubleshooting</h3>

            Follow these steps to solve the issue or report it to the team:
            <ul>
                <li>
                    If you have just updated Piwik to the latest version, please try to restart your web server.
                    This will clear the PHP opcache which may solve the problem.
                </li>
                <li>
                    If this is the first time you see this error, please try refresh the page.
                </li>
                <li>
                    <strong>If this error continues to happen</strong>, we appreciate if you send the
                    <a href=\"mailto:hello@piwik.org?subject=";
            // line 53
            echo twig_escape_filter($this->env, ("Fatal error in Piwik " . twig_escape_filter($this->env, (isset($context["piwikVersion"]) ? $context["piwikVersion"] : $this->getContext($context, "piwikVersion")), "url")), "html", null, true);
            echo "&body=";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "message", array()), "url"), "html", null, true);
            echo "%20in%20";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "file", array()), "url"), "html", null, true);
            echo "%20";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "line", array()), "url"), "html", null, true);
            echo "%20using%20PHP%20";
            echo twig_escape_filter($this->env, twig_constant("PHP_VERSION"), "html", null, true);
            echo "\">error report</a>
                    to the Piwik team.
                </li>
            </ul>
            <hr/>

        ";
        }
        // line 60
        echo "
        ";
        // line 61
        if ((isset($context["isSuperUser"]) ? $context["isSuperUser"] : $this->getContext($context, "isSuperUser"))) {
            // line 62
            echo "
            <h3>Further troubleshooting</h3>
            <p>
                If this error continues to happen, you may be able to fix this issue by disabling one or more of
                the Third-Party plugins. You can enable them again in the
                <a rel=\"noreferrer\" target=\"_blank\" href=\"index.php?module=CorePluginsAdmin&action=plugins\">Plugins</a>
                or <a target=\"_blank\" href=\"index.php?module=CorePluginsAdmin&action=themes\">Themes</a> page under
                settings at any time.

                ";
            // line 71
            if ((isset($context["pluginCausesIssue"]) ? $context["pluginCausesIssue"] : $this->getContext($context, "pluginCausesIssue"))) {
                // line 72
                echo "                    Based on the error message, the issue is probably caused by the plugin <strong>";
                echo twig_escape_filter($this->env, (isset($context["pluginCausesIssue"]) ? $context["pluginCausesIssue"] : $this->getContext($context, "pluginCausesIssue")), "html", null, true);
                echo "</strong>.
                ";
            }
            // line 74
            echo "            </p>
            <table>
                ";
            // line 76
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["plugins"]) ? $context["plugins"] : $this->getContext($context, "plugins")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            foreach ($context['_seq'] as $context["pluginName"] => $context["plugin"]) {
                if (($this->getAttribute($context["plugin"], "uninstallable", array()) && $this->getAttribute($context["plugin"], "activated", array()))) {
                    // line 77
                    echo "                    <tr ";
                    if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                        echo "style=\"background-color: #eeeeee\"";
                    }
                    echo ">
                        <td style=\"min-width:200px;\">
                            ";
                    // line 79
                    echo twig_escape_filter($this->env, $context["pluginName"], "html", null, true);
                    echo "
                        </td>
                        <td>
                            <a href=\"index.php?module=CorePluginsAdmin&action=deactivate&pluginName=";
                    // line 82
                    echo twig_escape_filter($this->env, $context["pluginName"], "html", null, true);
                    echo "&nonce=";
                    echo twig_escape_filter($this->env, (isset($context["deactivateNonce"]) ? $context["deactivateNonce"] : $this->getContext($context, "deactivateNonce")), "html", null, true);
                    echo "\"
                               target=\"_blank\">deactivate</a>
                        </td>
                    </tr>
                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['pluginName'], $context['plugin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "            </table>

            ";
            // line 89
            $context["uninstalledPluginsFound"] = false;
            // line 90
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["plugins"]) ? $context["plugins"] : $this->getContext($context, "plugins")));
            foreach ($context['_seq'] as $context["pluginName"] => $context["plugin"]) {
                if (($this->getAttribute($context["plugin"], "uninstallable", array()) &&  !$this->getAttribute($context["plugin"], "activated", array()))) {
                    // line 91
                    echo "                ";
                    $context["uninstalledPluginsFound"] = true;
                    // line 92
                    echo "            ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['pluginName'], $context['plugin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "
            ";
            // line 94
            if ((isset($context["uninstalledPluginsFound"]) ? $context["uninstalledPluginsFound"] : $this->getContext($context, "uninstalledPluginsFound"))) {
                // line 95
                echo "
                <p>
                    If this error still occurs after disabling all plugins, you might want to consider uninstalling some
                    plugins. Keep in mind: The plugin will be completely removed from your platform.
                </p>

                <table>
                    ";
                // line 102
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["plugins"]) ? $context["plugins"] : $this->getContext($context, "plugins")));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                foreach ($context['_seq'] as $context["pluginName"] => $context["plugin"]) {
                    if (($this->getAttribute($context["plugin"], "uninstallable", array()) &&  !$this->getAttribute($context["plugin"], "activated", array()))) {
                        // line 103
                        echo "                        <tr ";
                        if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                            echo "style=\"background-color: #eeeeee\"";
                        }
                        echo ">
                            <td style=\"min-width:200px;\">
                                ";
                        // line 105
                        echo twig_escape_filter($this->env, $context["pluginName"], "html", null, true);
                        echo "
                            </td>
                            <td>
                                <a href=\"index.php?module=CorePluginsAdmin&action=uninstall&pluginName=";
                        // line 108
                        echo twig_escape_filter($this->env, $context["pluginName"], "html", null, true);
                        echo "&nonce=";
                        echo twig_escape_filter($this->env, (isset($context["uninstallNonce"]) ? $context["uninstallNonce"] : $this->getContext($context, "uninstallNonce")), "html", null, true);
                        echo "\"
                                   target=\"_blank\" onclick=\"return confirm('";
                        // line 109
                        echo twig_escape_filter($this->env, twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_UninstallConfirm", $context["pluginName"])), "js"), "html", null, true);
                        echo "')\">uninstall</a>
                            </td>
                        </tr>
                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['pluginName'], $context['plugin'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 113
                echo "                </table>
            ";
            }
            // line 115
            echo "
        ";
        } elseif (        // line 116
(isset($context["isAnonymousUser"]) ? $context["isAnonymousUser"] : $this->getContext($context, "isAnonymousUser"))) {
            // line 117
            echo "
            <p>Please contact the system administrator, or login to Piwik to learn more.</p>

        ";
        } else {
            // line 121
            echo "            <p>
                If this error continues to happen you may want to send an
                <a href=\"mailto:";
            // line 123
            echo twig_escape_filter($this->env, (isset($context["emailSuperUser"]) ? $context["emailSuperUser"] : $this->getContext($context, "emailSuperUser")), "html", null, true);
            echo "?subject=";
            echo twig_escape_filter($this->env, ("Fatal error in Piwik " . twig_escape_filter($this->env, (isset($context["piwikVersion"]) ? $context["piwikVersion"] : $this->getContext($context, "piwikVersion")), "url")), "html", null, true);
            echo "&body=";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "message", array()), "url"), "html", null, true);
            echo "%20in%20";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "file", array()), "url"), "html", null, true);
            echo "%20";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->getAttribute((isset($context["lastError"]) ? $context["lastError"] : $this->getContext($context, "lastError")), "line", array()), "url"), "html", null, true);
            echo "%20using%20PHP%20";
            echo twig_escape_filter($this->env, twig_constant("PHP_VERSION"), "html", null, true);
            echo "\">error report</a>
                to your system administrator.
            </p>
        ";
        }
        // line 127
        echo "
        </div>

    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "@CorePluginsAdmin/safemode.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  296 => 127,  279 => 123,  275 => 121,  269 => 117,  267 => 116,  264 => 115,  260 => 113,  246 => 109,  240 => 108,  234 => 105,  226 => 103,  215 => 102,  206 => 95,  204 => 94,  201 => 93,  194 => 92,  191 => 91,  185 => 90,  183 => 89,  179 => 87,  162 => 82,  156 => 79,  148 => 77,  137 => 76,  133 => 74,  127 => 72,  125 => 71,  114 => 62,  112 => 61,  109 => 60,  91 => 53,  69 => 36,  64 => 34,  56 => 33,  53 => 32,  51 => 31,  19 => 1,);
    }
}
