var min_content_height = {
    _construct : function(){
        $(window).load(function() {
            var contentHeight = $(window).height() - ($('nav').outerHeight(true));
            $('.content').css('min-height',contentHeight);
            $('.content').css('padding-bottom',$('.footer').outerHeight(true));
        });
    }
};

min_content_height._construct();