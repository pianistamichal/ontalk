/**
 * Created by PianistaMichal on 14.02.2016.
 */

//For translate
function Translation() {
    this.message = {
            'Searching...' : 'Szukanie...',
            'Disconnect' : 'Rozłącz',
            'New talk' : 'Nowa rozmowa',
            'You are connected with partner' : 'Jesteś połączony z rozmówcą',
            'Connecting...' : 'Trwa łączenie',
            'Disconnected' : 'Rozłączony',
            "Has disconnected" : 'Rozłączył się'

    };
}

Translation.prototype.translate = function(toTranslate) {
    if(typeof this.message[toTranslate] != 'undefined') {
        return this.message[toTranslate];
    } else {
        return toTranslate;
    }
};