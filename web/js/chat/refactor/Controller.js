/**
 * Created by PianistaMichal on 14.02.2016.
 */

// Controller just for every action user can make ex.
//
// buttons click,
// keyboard press,
// form submit
function Controller() {
    var self = this;
    this.view = new View();
    this.connection = null;
    this.database = new Database();
    this.search = new Search(this);
    $(document).ready(function() {
        $('#search-button').click(function() {
            $(this).button('loading');
            self.search.newPartner(true);
        });

        $('#disconnect-button').click(function(){
            self.onDisconnectionButtonClick();
        });

        $('#newTalk-button').click(function() {
            self.onNewTalkButtonClick();
        });

        var input = $('#textInput');
        input.keydown(function(){
            self.onKeyPress();
        });

        $('#form').submit(function() {
            event.preventDefault();
            self.onSubmit();
        });

        input.focus();
    });
}

Controller.prototype.onRedirected = function() {
    var self = this;
    $(document).ready(function() {
        self.search.setRoom(room);
        self.search.setTopic(topic);
        self.connect();
    });
};

Controller.prototype.onDisconnectionButtonClick = function() {
    this.connection.disconnect();
    this.database.saveConversation();
    this.database.saveMessages();
    this.view.onDisconnected();
    this.connection = null;
};

Controller.prototype.onNewTalkButtonClick = function() {
    try {
        this.view.onFinding();
        this.search.newPartner();
    } catch(err) {
        this.view.onError(err);
    }
};

Controller.prototype.onFoundPartner = function () {
    this.connect();
};

Controller.prototype.connect = function () {
    this.view.onConnecting();
    this.connection = new Connection(this.view, this.database, this.search.getRoom());
    this.connection.connect();
    this.connection.handshake();
};

Controller.prototype.onKeyPress = function() {
    this.view.showCloud('L');
    this.connection.send({action : "writing"});
    if(this.isEnterPressed()) {
        $('#form').trigger('submit');
    }
};

Controller.prototype.isEnterPressed = function(e) {
    var keyCode = null;
    if (e!=null){
        if (window.event!=undefined){
            if (window.event.keyCode) keyCode = window.event.keyCode;
            else if (window.event.charCode) keyCode = window.event.charCode;
        } else {
            keyCode = e.keyCode;
        }
    }
    return (keyCode == 13);
};

Controller.prototype.onSubmit = function() {
    var data = $('#textInput');
    this.database.addMessage("L", data.val());
    this.view.showMessage("L", data.val());
    this.connection.send({action : "message", message : data.val()});
    data.val('');
};