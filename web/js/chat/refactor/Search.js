/**
 * Created by PianistaMichal on 14.02.2016.
 */

//Searching new partner and set room & topic to attributes, can get attributes value back
function Search(controller) {
    this.room = "";
    this.topic = "";
    this.controller = controller;
}

Search.prototype.newPartner = function(redirect) {
    var self = this;
    $.ajax({
        url: webPath + '/index.php/chat/search',
        success: function(data) {
            if(data['isConnected']) {
                if(redirect) {
                    window.location.replace(webPath + "/index.php/chat/index");
                } else {
                    self.setRoom(data['room']);
                    self.setTopic(data['topic']);
                    self.controller.onFoundPartner();
                }
            } else {
                setTimeout(function() {self.newPartner(redirect)}, 500);
            }
        },
    });
};

Search.prototype.getRoom = function() {
    return this.room;
};

Search.prototype.getTopic = function() {
    return this.topic;
};

Search.prototype.setTopic = function(topic) {
    this.topic = topic;
};

Search.prototype.setRoom = function(room) {
    this.room = room;
};