/**
 * Created by PianistaMichal on 14.02.2016.
 */

//Database for save conversation and messages from conversation between users to database
function Database() {
    this.conversation = [];
    this.messages = [];
}

Database.prototype.addMessage = function (who, msg) {
    if(who == "L") {
        this.messages.push(msg);
    }
    this.conversation[this.conversation.length] = [who, msg];
};

Database.prototype.saveConversation = function () {
    console.log(this.conversation);
    if(this.conversation.length != 0) {
        var data = 'data=' + this.conversation;
        $.ajax({
            method: "POST",
            url: webPath + '/index.php/chat/save-conversation',
            data: data
        });
    }
};

Database.prototype.saveMessages = function() {
    console.log(this.messages);
    if(this.messages.length != 0) {
        var data = 'data=' + this.messages;
        $.ajax({
            method: "POST",
            url: webPath + "/index.php/chat/save-messages",
            data: data
        });
    }
};