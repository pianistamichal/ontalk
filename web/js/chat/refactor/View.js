/**
 * Created by PianistaMichal on 14.02.2016.
 */

// View for everything You see ex:
// writing clouds,
// messages,
// visual changes after user action
function View() {
    this.cloud = {"L" : 0, "R" : 0};
}

View.prototype.onConnecting = function() {
    this.clearScreen();
    this.showMessage("L", "I found somebody just for You. Connecting...");
};

View.prototype.onConnected = function() {
    $('#newTalk-button').css("display", "none");
    $('#disconnect-button').css("display", "inline");
    this.clearScreen();
    this.showMessage("L", "Connected, start chatting with stranger. Say Hi.");
    $('#textInput').prop('disabled', false);
};

View.prototype.onDisconnected = function () {
    $('#disconnect-button').css("display", "none");
    $('#newTalk-button').css("display", "inline");
    $('#textInput').prop('disabled', true);
    this.showMessage("L", "Disconnected");
};

View.prototype.showTopic = function(topic) {
    $(".header").find('h3').text(topic);
};

View.prototype.showMessage = function (who, data) {
    var div  = $('div.box');
    var toPrint = '<div class="Area"> <blockquote class="example-obtuse ' + who + '">'+ data +'</blockquote></div>';

    div.append(toPrint);
    div.scrollTop(div.scrollTop() + 10000);
    return toPrint;
};

View.prototype.showCloud = function(who) {
    var self = this;
    if(this.cloud[who] <= 1) {
        this.cloud[who]++;
        if(this.cloud[who] == 1) {
            $('.cloud_img#' + who).css("visibility","visible");
            setTimeout(function(){
                if(self.cloud[who] == 1) {
                    self.cloud[who] = 0;
                    $('.cloud_img#' + who).css("visibility","hidden");
                } else {
                    self.cloud[who] = 0;
                    self.showCloud(who);
                }
            }, 400)
        }
    }
};

View.prototype.onFinding = function() {
    this.clearScreen();
    this.showMessage("L", "Searching...");
};

View.prototype.onError = function() {
    this.showMessage("L", "There is some error");
};

View.prototype.clearScreen = function () {
    $('div.box').text('');
};