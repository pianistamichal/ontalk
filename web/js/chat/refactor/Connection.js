/**
 * Created by PianistaMichal on 14.02.2016.
 */

// Connection for:
//
// connecting/disconnecting with server,
// sending/receiving messages,
// switching between actions received from server,
// preparing handshake with partner
function Connection(view, database, room) {
    this.port = 3030;
    this.host = "127.0.0.1";
    this.room = room;
    this.socket = null;
    this.view = view;
    this.database = database;
}

Connection.prototype.connect = function() {
    var self = this;
    try {
        this.socket = new Primus(this.host + ":" + this.port);
        this.socket.on('data', function receive(data) {
            switch(data['action']) {
                case "message" :
                    self.database.addMessage("R", data['message']);
                    self.view.showMessage("R", data['message']);
                    break;
                case "peopleInRoom" :
                    setTimeout(function(){self.handshake(data['peopleInRoom'])}, 500);
                    break;
                case "leave" :
                    self.view.onDisconnected();
                    self.disconnect();
                    break;
                case "writing" :
                    self.view.showCloud('R');
                    break;
            }
        });
        this.socket.on('end', function() {
           // self.disconnect();
            //self.view.onDisconnected();
        });
        this.send({action : "join", room : this.room});
    } catch(err) {
        throw err;
    }
};

Connection.prototype.handshake = function(peopleInRoom) {
    if(typeof peopleInRoom != "undefined") {
        if(peopleInRoom < 2) {
            this.handshake();
        } else {
            this.view.onConnected();
        }
        //ends handshake
        return true;
    } else {
        //starts handshake procedure
        this.send({action : "peopleInRoom"});
    }
};

Connection.prototype.disconnect = function() {
    try {
        this.send({ action: 'leave' });
        this.socket.end();
    } catch(err) {
        throw err;
    }
};

Connection.prototype.send = function(data) {
    this.socket.write(data);
};