<?php

namespace app\controllers;

use Yii;
use app\models\activeRecord\Conversation;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\form\ContactForm;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->saveIncomingMessage();
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionMessages()
    {
        if(Yii::$app->user->isGuest) {
            $this->goHome();
        }
        $ctc = new Conversation();
        $provider = new ActiveDataProvider([
            'query'=> $ctc->find()->where(['username1'=> Yii::$app->user->identity->username]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        //creating new provider models
        $before = $provider->getModels();
        //cut text to display for every existing conversation
        foreach($before as $b){
            $b->text = $ctc->craftTextToDisplay($b->text);
        }
        $provider ->setModels($before);

        return $this->render('messages', [
            'provider'=>$provider,
        ]);
    }

    public function actionUsermessages($id){
        if(Yii::$app->user->isGuest) {
            $this->goHome();
        }
        $conversation = new Conversation();
        $dbString = Conversation::find()->where(['ID'=>$id])->one();
        $text = $conversation->convertTextToDisplay($dbString->text);

        return $this->render('usermessages',[
        'text'=>$text,]);
    }
    public function actionRules(){
        return $this->render('rules');
    }

}

