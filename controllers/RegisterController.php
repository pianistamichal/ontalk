<?php
/**
 * Created by PhpStorm.
 * User: Phate-Admin
 * Date: 2015-08-20
 * Time: 13:38
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\form\RegisterForm;
use app\models\activeRecord\UserPersonalized;

class RegisterController extends Controller
{
    public function  actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            //register user
            $model->register();

            $userPersonalizeInitiate = new UserPersonalized();
            $userPersonalizeInitiate->username = $model->login;
            $userPersonalizeInitiate->save();

            //info
            Yii::$app->session->setFlash('RegisterCorrect', 'User has been registred successfully.');
        }

        return $this->render('register', [
            'model' => $model
        ]);
    }

}