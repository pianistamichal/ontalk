<?php
/**
 * Created by PhpStorm.
 * User: Phate-Admin
 * Date: 2015-08-20
 * Time: 13:38
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\form\LoginForm;

class LoginController extends Controller
{
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}