<?php

namespace app\controllers;


use app\models\AnswerQuestion;
use app\models\form\AdminPanelForm;
use Yii;
use yii\web\Controller;
use app\models\activeRecord\Contact;
use yii\data\ActiveDataProvider;


class AdminpanelController extends Controller
{
     public function actionAddTopics()
     {
         //d
         if(Yii::$app->user->isGuest || Yii::$app->user->identity->user_role==0) {
             $this->goHome();
         }
         $model = new AdminPanelForm();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
             $model->addNewTopic();
             Yii::$app->session->setFlash("topic_added","Topic: '".$model->topic."' has been added");
         }
         return $this->render('addtopics', [
             'model' => $model,
             ]);
     }
     public function actionRemoveTopics(){
         if(Yii::$app->user->isGuest || Yii::$app->user->identity->user_role==0) {
             $this->goHome();
         }
         $model = new AdminPanelForm();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
             if($model->removeTopic()){
                 Yii::$app->session->setFlash("topic_removed","Topic: '".$model->topic."' has been removed");
             }
             else{
                 Yii::$app->session->setFlash("topic_not_removed","Can not remove: '".$model->topic."'. Are you sure it does exist?");
             }
         }
         return $this->render('removetopics', [
             'model' => $model,
         ]);

     }
     public function actionMessages(){
         if(Yii::$app->user->isGuest || Yii::$app->user->identity->user_role==0) {
             $this->goHome();
         }
         $ctc = new Contact();
         $provider = new ActiveDataProvider([
             'query'=> $ctc->find(),
             'pagination' => [
                 'pageSize' => 100,
             ],
         ]);

         return $this->render('messages', [
             'provider'=>$provider,
         ]);
     }
    public function actionDeleterecord($id){
        if(Yii::$app->user->isGuest || Yii::$app->user->identity->user_role==0) {
            $this->goHome();
        }
        $ctc = Contact::find()->where(['ID'=>$id])->one();
        $ctc->delete();
            return $this->render("deleterecord");
    }
    public function actionAnswerQuestion($id){
        if(Yii::$app->user->isGuest || Yii::$app->user->identity->user_role==0) {
            $this->goHome();
        }
        $model = new AnswerQuestion();
        //taking conversation info
        $info = Contact::find()->where(['ID'=>$id])->one();

        if($model->load(Yii::$app->request->post())){
            $model->sendMail($info->email, $info->subject);
            Yii::$app->session->setFlash("email_sent","Email has been sent");
        }

        return $this->render('answerquestion', [
            'info'=>$info,
            'model'=>$model,
        ]);
    }
}