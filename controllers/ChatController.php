<?php
namespace app\controllers;


use app\models\chat\SaveConversation;
use app\models\chat\SearchRandom;
use app\models\activeRecord\Topics;
use app\models\activeRecord\UserPersonalized;
use yii\web\Controller;
use yii\web\JsExpression;
use \yii\web\Session;
use Yii;

class ChatController extends Controller {
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex(){
        $session = new Session();
        $session->open();

        if(isset($session['room'])) {
            return $this->render('index');
        } else {
            return $this->goHome();
        }
    }

    public function actionSaveConversation() {
        if(!Yii::$app->user->isGuest) {
            $model = new SaveConversation();
            $model->deleteOldConversation();
            $model->saveConversation();
            return true;
        } else {
            return false;
        }
    }

    //ajax call
    public function actionEndSearching() {
        $model = new SearchRandom();
        $model->deleteSearcherFromWaiting();
    }

    //ajax call
    public function actionSearch() {
        Yii::$app->response->format = 'json';
        $model = new SearchRandom();
        $model->isUserWaiting() ? $model->waitingChatterAction() : $model->newChatterAction();
        $toReturn = [];
        $toReturn['isConnected'] = $model->getIsConnected();
        $toReturn['room'] = $model->getRoom();
        $toReturn['topic'] = $model->getTopic();

        return new JsExpression(json_encode($toReturn));
    }

    public function actionSaveMessages() {
        if(!Yii::$app->user->isGuest) {
            $model = UserPersonalized::findOne(['username' => Yii::$app->user->identity->username]);
            if($model != null) {
                $model->text .= $_POST['data'];
                $model->save();
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionSetRoom() {
        $session = new Session();
        $session->open();
        $session['room'] = $_POST['data'];
    }

    public function actionGetRoom() {
        $session = new Session();
        $session->open();
        return $session['room'];
    }

    public function actionTopic() {
        return Topics::randomTopic();
    }

}