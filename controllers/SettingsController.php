<?php
/**
 * Created by PhpStorm.
 * User: Phate-Admin
 * Date: 2015-08-20
 * Time: 12:52
 */

namespace app\controllers;
use app\models\form\DeleteAccountForm;
use app\models\form\EmailForm;
use Yii;
use app\models\form\PasswordForm;
use yii\web\Controller;

class SettingsController extends Controller
{
    function actionEmail(){
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new EmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setNewEmail();
            Yii::$app->session->setFlash('EmailChange', 'Email has been changed correctly!.');

            return $this->render('email', [
                'model' => $model,
            ]);
        }else {
            return $this->render('email', [
                'model' => $model,
            ]);
        }
    }
    public function actionPassword()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
         $model = new PasswordForm();
         if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->SetNewPassword();
                 Yii::$app->session->setFlash('PasswordChange', 'Password has been changed correctly!.');

                 return $this->render('password', [
                 'model' => $model,
                 ]);
         }else {
            return $this->render('password', [
                 'model' => $model,
            ]);
         }
    }
    public function actionDeleteAccount()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new DeleteAccountForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           if($model->deleteAccount()){
               $this->goHome();
           }
            else{
                return $this->render('deleteaccount', [
                    'model' => $model,
                ]);
            }
        }
            return $this->render('deleteaccount', [
                'model' => $model,
            ]);
    }
    public function actionLanguage(){
        Yii::$app->language = 'en';
    }

}