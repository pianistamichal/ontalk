<?php
//namespace app\AccountSettings\models;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'password_change';
//$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::getAlias('@web') ?>/js/min-height-image.js"></script>

<div class="site-password_change">
    <h3><?= Yii::t('app', 'Change your password:') ?></h3>
    <div class="container">
        <div class="col-md-8 cnt-pg">
            <?php $form = ActiveForm::begin([
                'id' => 'password_change',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-4 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'old_password')->passwordInput() ?>
            <?= $form->field($model, 'new_password')->passwordInput() ?>
            <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>
            <?php
            if(Yii::$app->session->hasFlash('PasswordChange'))
                echo "<div class = 'alert alert-success'>" . Yii::$app->session->getFlash('PasswordChange')." </div>";
            ?>




            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-11">
                    <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary', 'name' => 'change-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/pass.jpg');background-size:cover;">

        </div>
    </div>
</div>