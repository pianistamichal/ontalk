<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Account_delete';
//$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::getAlias('@web') ?>/js/min-height-image.js"></script>
<div class="site-Account_delete">
    <h3><?= Yii::t('app', 'Panel usuwania konta:') ?></h3>
        <div class="container">
            <div class="col-md-8 cnt-pg">

                <?php $form = ActiveForm::begin([
                    'id' => 'Account_delete',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                        'labelOptions' => ['class' => 'col-md-4 control-label'],
                    ],
                ]); ?>


                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <div class="col-lg-offset-4 col-lg-11">
                        <?= Html::submitButton(Yii::t('app', 'Remove your account'), ['class' => 'btn btn-primary', 'name' => 'reomve-button']) ?>
                    </div>
                </div>
                <div>
                    <p><h4><b> <?= Yii::t('app','Be sure before you delete your account'); ?></b></h4></p>
                    <p><h4><b> <?= Yii::t('app','Changes can not be undone!'); ?></b></h4></p>
                </div>
                <?php
                if(Yii::$app->session->hasFlash('Admin_Remove'))
                    echo "<div class = 'alert alert-success'>" . Yii::$app->session->getFlash('Admin_Remove')." </div>";
                ?>


                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/delete.jpg');background-size:cover;">

            </div>
        </div>
    </div>
</div>
