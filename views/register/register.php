<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('app', 'Register');
//$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::getAlias('@web') ?>/js/min-height-image.js"></script>
<div class="site-register">
    <h3><?=$this->title?></h3>

    <div class="container">
        <div class="col-md-8 cnt-pg">

            <?php $form = ActiveForm::begin([
                'id' => 'register-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-4 control-label'],
                ],
            ]);
            ?>

            <?= $form->field($model,'login')->textInput() ?>

            <?= $form->field($model,'password')->passwordInput() ?>

            <?= $form->field($model,'password_repeat')->passwordInput() ?>

            <?= $form->field($model,'email')->textInput() ?>

            <?= $form->field($model, 'captcha')->widget(Captcha::className()) ?>

            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-11">
                    <?= Html::submitButton(Yii::t('app', 'Send'),['class'=>'btn btn-primary','name' => 'submit-button']);?>
                </div>
            </div>

            <?php
            if(Yii::$app->session->hasFlash('RegisterCorrect'))
                echo "<div class = 'alert alert-success'>" . Yii::$app->session->getFlash('RegisterCorrect')." </div>";
            ?>
            <?php ActiveForm::end(); ?>
            <div>
                <p><h4><b> <?= Yii::t('app','Did you know?'); ?></b></h4></p>
                <p><h5><b> <?= Yii::t('app','Only Registred accounts have acces to conversation history.'); ?></b></h5></p>
                <p><h5><b> <?= Yii::t('app','New alghoritm that search people depending on your personality soon!'); ?></b></h5></p>
            </div>

        </div>
        <div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/register.jpg');background-size:cover;">

        </div>
    </div>

</div>


