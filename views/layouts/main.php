<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$preferredLanguage = Yii::$app->request->getPreferredLanguage(['pl', 'en']);
Yii::$app->language = $preferredLanguage;
$session = new \yii\web\Session();
$session->open();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?> onload="resizeContent()" onresize="resizeContent()"">
    <head>
        <title>OnTalk - talking with strangers</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="index, follow">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Free Online chat. Talk to strangers from any place you want. Nice alghoritms that search interesting people just for you.">
        <meta name="keywords" content="Chat, Online, Strangers, chat with people, fun, free, ontalk, Online chat, Free chat, Web chat, online chat rooms,  " />
        <script type="application/x-javascript"> addEventListener("load", function() {setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <meta charset utf="8">
        <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/bootstrap.min.js"></script>
        <!--bootstrap-->
        <link href="<?= Yii::getAlias('@web') ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!--flexslider-css-->
        <link href="<?= Yii::getAlias('@web') ?>/css/flexslider.css" rel='stylesheet' type='text/css' />
        <!--coustom css-->
        <link href="<?= Yii::getAlias('@web') ?>/css/style.css" rel="stylesheet" type="text/css"/>
        <!--fonts-->
        <link href='http://fonts.googleapis.com/css?family=Coda:400,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Jockey+One' rel='stylesheet' type='text/css'>
        <!--script-->
        <script src="<?= Yii::getAlias('@web') ?>/js/move-top.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/easing.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/modernizr.custom.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/min-content-height.js"></script>
        <script src="<?= Yii::getAlias('@web') ?>/js/jquery.tabSlideOut.v1.3.js"></script>

        <!--script-->
        <script type="text/javascript">
            $(document).ready(function($) {
                $(".scroll").click(function(event){
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
                });
                $(function(){
                    $('.slide-out-div').tabSlideOut({
                        tabHandle: '.handle',                     //class of the element that will become your tab
                        pathToTabImage: '<?= Yii::getAlias('@web') ?>/img/ankieta.png', //path to the image for the tab //Optionally can be set using css
                        imageHeight: '122px',                     //height of tab image           //Optionally can be set using css
                        imageWidth: '40px',                       //width of tab image            //Optionally can be set using css
                        tabLocation: 'left',                      //side of screen where tab lives, top, right, bottom, or left
                        speed: 300,                               //speed of animation
                        action: 'click',                          //options: 'click' or 'hover', action to trigger animation
                        topPos: '200px',                          //position from the top/ use if tabLocation is left or right
                        leftPos: '20px',                          //position from left/ use if tabLocation is bottom or top
                        fixedPosition: true                      //options: true makes it stick(fixed position) on scroll
                    });

                });
            });
        </script>

        <style type="text/css">
            .slide-out-div {
                padding: 20px;
                width: 250px;
                border: 1px solid #29216d;
                background: white;
                z-index:1;
            }
        </style>
    </head>

    <body><?php $this->beginBody() ?>
        <div class = 'site' id="home">
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?=Url::to(['/site/index'])?>">OnTalk</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right margin-top cl-effect-2">
                            <?php
                                if(Yii::$app->user->isGuest) {
                                    echo '<li><a href="'.Url::to(["/site/index"]).'"><span data-hover="'.Yii::t('app', 'Home').'">'.Yii::t('app', 'Home').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/about"]).'"><span data-hover="'.Yii::t('app', 'About').'">'.Yii::t('app', 'About').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/contact"]).'"><span data-hover="'.Yii::t('app', 'Contact').'">'.Yii::t('app', 'Contact').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/register/register"]).'"><span data-hover="'.Yii::t('app', 'Register').'">'.Yii::t('app', 'Register').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/login/login"]).'"><span data-hover="'.Yii::t('app', 'Login').'">'.Yii::t('app', 'Login').'</span></a></li>';
                                } else if(Yii::$app->user->identity->user_role==0) {

                                    echo '<li><a href="'.Url::to(["/site/index"]).'"><span data-hover="'.Yii::t('app', 'Home').'">'.Yii::t('app', 'Home').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/about"]).'"><span data-hover="'.Yii::t('app', 'About').'">'.Yii::t('app', 'About').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/contact"]).'"><span data-hover="'.Yii::t('app', 'Contact').'">'.Yii::t('app', 'Contact').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/messages"]).'"><span data-hover="'.Yii::t('app', 'Conversation').'">'.Yii::t('app', 'Conversation').'</span></a></li>';
                                    echo '<li class="dropdown">
                                                <a href="about.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-hover="'.Yii::t('app', 'Settings').'">'.Yii::t('app', 'Settings').'</span><span class="caret"></span></a>
                                            <ul class="dropdown-menu">';
                                    echo '<li><a href="'.Url::to(["/settings/password"]).'"><span data-hover="'.Yii::t('app', 'Change password').'">'.Yii::t('app', 'Change password').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/settings/email"]).'"><span data-hover="'.Yii::t('app', 'Change email adress').'">'.Yii::t('app', 'Change email adress').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/settings/delete-account"]).'"><span data-hover="'.Yii::t('app', 'Delete your account').'">'.Yii::t('app', 'Delete your account').'</span></a></li>';
                                    echo '</ul>
                                            </li>';
                                    echo '<li><a href="'.Url::to(["/login/logout"]).'"><span data-hover="'.Yii::t('app', 'Logout')." (".Yii::$app->user->identity->username .")".'">'.Yii::t('app', 'Logout')." (".Yii::$app->user->identity->username .")".'</span></a></li>';
                                }
                                else if(Yii::$app->user->identity->user_role ==1){

                                    echo '<li><a href="'.Url::to(["/site/index"]).'"><span data-hover="'.Yii::t('app', 'Home').'">'.Yii::t('app', 'Home').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/about"]).'"><span data-hover="'.Yii::t('app', 'About').'">'.Yii::t('app', 'About').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/site/messages"]).'"><span data-hover="'.Yii::t('app', 'Conversation').'">'.Yii::t('app', 'Conversation').'</span></a></li>';
                                    echo '<li class="dropdown">
                                                <a href="about.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-hover="'.Yii::t('app', 'Settings').'">'.Yii::t('app', 'Settings').'</span><span class="caret"></span></a>
                                            <ul class="dropdown-menu">';
                                    echo '<li><a href="'.Url::to(["/settings/password"]).'"><span data-hover="'.Yii::t('app', 'Change password').'">'.Yii::t('app', 'Change password').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/settings/email"]).'"><span data-hover="'.Yii::t('app', 'Change email adress').'">'.Yii::t('app', 'Change email adress').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/settings/delete-account"]).'"><span data-hover="'.Yii::t('app', 'Delete your account').'">'.Yii::t('app', 'Delete your account').'</span></a></li>';
                                    echo '</ul>
                                            </li>';
                                    echo '<li class="dropdown">
                                                <a href="about.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-hover="'.Yii::t('app', 'Admin Panel').'">'.Yii::t('app', 'Admin Panel').'</span><span class="caret"></span></a>
                                            <ul class="dropdown-menu">';
                                    echo '<li><a href="'.Url::to(["/adminpanel/add-topics"]).'"><span data-hover="'.Yii::t('app', 'Add new topic').'">'.Yii::t('app', 'Add new topic').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/adminpanel/remove-topics"]).'"><span data-hover="'.Yii::t('app', 'Remove topic').'">'.Yii::t('app', 'Remove topic').'</span></a></li>';
                                    echo '<li><a href="'.Url::to(["/adminpanel/messages"]).'"><span data-hover="'.Yii::t('app', 'Messages').'">'.Yii::t('app', 'Messages').'</span></a></li>';
                                    echo '<li><a href="'.Url::base(). "/piwik".'"><span data-hover="Piwik">Piwik</span></a></li>';
                                    echo '</ul>
                                            </li>';
                                    echo '<li><a href="'.Url::to(["/login/logout"]).'"><span data-hover="'.Yii::t('app', 'Logout')." (".Yii::$app->user->identity->username ." - Admin)".'">'.Yii::t('app', 'Logout')." (".Yii::$app->user->identity->username ." - Admin)".'</span></a></li>';
                                }
                            ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div><!-- /.navbar-collapse -->
                    <div class="clearfix"></div>
                </div><!-- /.container-fluid -->
            </nav>

            <div class = "content">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
            <div class="slide-out-div">
                <a class="handle" href="http://link-for-non-js-users.html">Content</a>
                <h3>Feedback</h3>
                <p>Tworzymy ten serwis z myślą o Tobie. Teraz pomyśl czego potrzebujesz i daj nam znać, wypełniając tę
                    <a href="https://docs.google.com/forms/d/1x2yfWUma-3wvBTK59w0OuVNYW3_PzzkLMeDqwJmEs8M/viewform">ankietę</a>
                </p>
            </div>
            <div class="footer">
                <div class="container">
                    <div class="social">
                        <ul>
                            <li><a href="https://www.facebook.com/OnTalkpl-1623259174629029/timeline/" class="face"></a></li>
                        </ul>
                    </div>
                    <div class="copy-rt">
                        <p><?= Yii::t('app', 'Starting conversation is equal to accepting ')?> <u><a href="http://ontalk.pl/index.php/site/rules"><?=Yii::t('app', 'site rules');?></a></u></p>
                        <p><?= Yii::t('app', 'Created by Michal Blocinski & Przemyslaw Petka') ?></p>
                    </div>
                </div>
            </div>

            <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        </div>
    <!-- Piwik -->
    <script type="text/javascript">
        var _paq = _paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u="//ontalk.pl/piwik/";
            _paq.push(['setTrackerUrl', u+'piwik.php']);
            _paq.push(['setSiteId', 1]);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
    </script>
    <noscript><p><img src="//ontalk.pl/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
    <!-- End Piwik Code -->
    <?php $this->endBody() ?></body>
</html>
<?php $this->endPage() ?>
