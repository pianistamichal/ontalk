<?php
use app\models\Chat;
use yii\web\Session;
/* @var $this yii\web\View */
$this->title = 'Chat';
$session = new Session();
$session->open();
?>
<head>
    <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web') ?>/css/chat.css">
    <script src="<?= Yii::getAlias('@web') ?>/primus/node_modules/primus.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Connection.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Controller.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Database.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Search.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Translation.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/View.js"></script>
    <script>

        //settings
        var room = "<?= $session['room'] ?>";
        var topic = "<?= $session['topic'] ?>";
        var webPath = '<?= Yii::getAlias('@web') ?>';
        var controller = new Controller();
        controller.onRedirected();
        var audio = new Audio('<?= Yii::getAlias('@web') ?>/music/beep.mp3');
    </script>
</head>

<div class="header"><h3 style="margin-top: 0"></h3></div>
<div class="me">
    <img src="<?= Yii::getAlias('@web') ?>/img/me_chat_cloud.png" width="40%" height="20%" align="right" style="margin-right: 20px" class="cloud_img" id="L">
    <img src="<?= Yii::getAlias('@web') ?>/img/me.png" width="100%" height="80%"/>
</div>

<div class="chat">
    <div class="box"></div>
    <div class="input">
        <form id = 'form'>
            <input type="text" id="textInput">
            <button type="button" id="disconnect-button" class="btn btn-primary" autocomplete="off"><?= Yii::t('app', 'Disconnect') ?></button>
            <button type="button" id="newTalk-button" style="display:none" data-loading-text="<?= Yii::t('app', 'Searching...')?>" class="btn btn-primary" autocomplete="off"><?= Yii::t('app', 'New talk') ?></button>
        </form>
    </div>
</div>

<div class="partner">
    <img src="<?= Yii::getAlias('@web') ?>/img/partner_chat_cloud.png" width="40%" height="20%" align="left" style="margin-left: 20px" class="cloud_img" id="R">
    <img src="<?= Yii::getAlias('@web') ?>/img/partner.png" width="100%" height="80%"/>
</div>