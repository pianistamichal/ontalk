<?php
use yii\helpers\Html;
use app\models\Topics;
?>
<header>
    <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web') ?>/css/style.css">
</header>
    <h3><?= Yii::t('app','Site Rules'); ?></h3>
<ol class="rules">
    <li> <?= Yii::t('app','1)Rule'); ?> </li>
    <li> <?= Yii::t('app','2)Rule'); ?> </li>
    <li> <?= Yii::t('app','3)Rule'); ?> </li>
    <li> <?= Yii::t('app','4)Rule'); ?> </li>
    <li> <?= Yii::t('app','5)Rule'); ?> </li>
    <li> <?= Yii::t('app','6)Rule'); ?> </li>
    <li> <?= Yii::t('app','7)Rule'); ?> </li>
    <li> <?= Yii::t('app','8)Rule'); ?> </li>
    <li> <?= Yii::t('app','9)Rule'); ?> </li>
    <li> <?= Yii::t('app','10)Rule'); ?> </li>
    <li> <?= Yii::t('app','11)Rule'); ?> </li>
    <li> <?= Yii::t('app','12)Rule'); ?> </li>
    <li> <?= Yii::t('app','13)Rule'); ?> </li>
    <li> <?= Yii::t('app','14)Rule'); ?> </li>
</ol>