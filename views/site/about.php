<!doctype html>
<html>
	<body>
		<div class="header" id="home">
			<div class="about-page">
			<div class="about">
				<h3><?=Yii::t('app', 'About')?></h3>
				<div class="container">
				<!--	<div class="col-md-4 awrd">
						<h4>Awards</h4>
							<div class="awd-icn-txt">
								<div class="col-md-3 awd-icn">
									<span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
								</div>
								<div class="col-md-9 awd-txt">
									<h4>Lorem Ipsum has been the industry's</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="awd-icn-txt">
								<div class="col-md-3 awd-icn">
									<span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
								</div>
								<div class="col-md-9 awd-txt">
									<h4>Lorem Ipsum has been the industry's</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="awd-icn-txt">
								<div class="col-md-3 awd-icn">
									<span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
								</div>
								<div class="col-md-9 awd-txt">
									<h4>Lorem Ipsum has been the industry's</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						<div class="clearfix"></div>
					</div> -->
					<div class="col-md-8 wh-we">
						<h4><?= (Yii::t('app', 'Who We Are'))?></h4>
						<div class="col-md-4 txt0">
							<h5><?= (Yii::t('app', 'Programing Its Not Only Our Job, But Passion'))?></h5>
							<P><?= (Yii::t('app', 'We are young people, who study on the 3rd year of Physics - IT on Technical University of Gdansk. We are creative and hard-working, because we know its the only way to success. After work we enjoy time with nature. Its our internal love'))?></P>
						<!--	<button type="button" class="btn btn-info mrg">Read more</button> -->
						</div>
						<div class="col-md-8 txt0">
							<img src="<?= Yii::getAlias('@web') ?>/img/job.jpg" alt="" class="img-responsive"/>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="our-team">
				<h3><?= (Yii::t('app', 'Our Team'))?></h3>
				<div class="container">
					<div class="team-gallery">
						<ul>
							<li class="col-md-2 tem-gal">
								<img src="<?= Yii::getAlias('@web') ?>/img/michau.jpg" alt="" class="img-responsive"></img>
								<h4>Michal </h4>
							</li>
							<li class="col-md-2 tem-gal">
								<img src="<?= Yii::getAlias('@web') ?>/img/przemek.jpg" alt="" class="img-responsive"></img>
								<h4>Przemek</h4>
							</li>
							<div class="clearfix"></div>
						</ul>
					</div>
				</div>
			</div>
		<!--	<div class="test-monials">
				<h3>Testimonials</h3>
				<div class="container">
					<div class="col-md-6 txt1">
						<h4>Lorem Ipsum has been the industry's standard dummy text since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</h4>
						<p>andrew wayne / <a href="#">demolink.org</a></p>
					</div>
					<div class="col-md-6 txt1">
						<h4>Lorem Ipsum has been the industry's standard dummy text since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</h4>
						<p>eva green / <a href="#">demolink.org</a></p>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6 txt1">
						<h4>Lorem Ipsum has been the industry's standard dummy text since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</h4>
						<p>cody / <a href="#">demolink.org</a></p>
					</div>
					<div class="col-md-6 txt1">
						<h4>Lorem Ipsum has been the industry's standard dummy text since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</h4>
						<p>cody / <a href="#">demolink.org</a></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div> -->
			</div>
			<div class="footer">
			<div class="container">
				<div class="social">
					<ul>
						<li><a href="#" class="face"></a></li>
						<li><a href="#" class="twit"></a></li>
						<li><a href="#" class="insta"></a></li>
						<li><a href="#" class="gplus"></a></li>
						<li><a href="#" class="dribl"></a></li>
					</ul>
				</div>
				<div class="copy-rt">
					<p>&#169; 2015 Archer Design by <a href="http://www.w3layouts.com" target="_blank">w3layouts</a></p>
				</div>
			</div>
			</div>
			<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</body>
</html>
