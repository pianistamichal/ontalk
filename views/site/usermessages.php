<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Usermessages');
?>
    <head>
          <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web') ?>/css/chat.css">
    </head>

    <div class="header">
        <h3><?= Yii::t('app','Conversation:') ?></h3>
    </div>

    <div class="me">
        <img src="<?= Yii::getAlias('@web') ?>/pic/me_chat_cloud.png" width="40%" height="20%" align="right" style="margin-right: 20px" class="cloud_img" id="first">
    </div>

    <div class="chat" >
        <div class="box">

            <?= $text; ?>
        </div>
    </div>
    <div class="partner">
        <img src="<?= Yii::getAlias('@web') ?>/pic/partner_chat_cloud.png" width="40%" height="20%" align="left" style="margin-left: 20px" class="cloud_img" id="second">
    </div>

<script>
    $(document).ready(function(){

        var contentHeight = $(window).height() - ($('nav').outerHeight(true) + $('.footer').outerHeight(true));
        var chatHeight = contentHeight - $('.header').find('h3').outerHeight(true)
        var boxHeight = chatHeight - $('#textInput').outerHeight(true) - $('#disconnect-button').outerHeight(true);

        $('.content').height(contentHeight);
        $('.chat').height(chatHeight);
        $('.box').height(boxHeight-20);
    });
</script>

