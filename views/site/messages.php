<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


$this->title = Yii::t('app', 'Messages');


?>
<div class="site-about">

    <head>
        <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web') ?>/css/contact.css">
    </head>
    <h3><?= Html::encode($this->title) ?></h3>
    <div class="dataGridView">
        <?php
        echo GridView::widget([
            'dataProvider' => $provider,
            'showHeader'=>false,
            'showFooter'=>false,
            'summary'=>"",
            'layout'=>"{items}",

            'columns' => [
                [
                    'attribute' => 'text',
                    'format' => 'text',
              //      'label' => Yii::t('app', "Messages"),
                    'contentOptions' => ['class' => 'text-wrap'],
                ],
                //action
                [
                    'class' => 'yii\grid\ActionColumn',
                //    'header'=>Yii::t('app', 'Action'),
               //     'headerOptions' => ['width' => '70'],
                    'template' => '{usermessages}',
                    'buttons' => [
                        'usermessages' => function ($url, $model) {
                            return Html::a('<span class="fa fa-search"></span>'. Yii::t('app', 'Whole Conversation'), $url, [
                                'class'=>'btn btn-primary btn-xs',
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action ==='usermessages') {
                            return Yii::$app->getUrlManager()->createUrl(['site/'.$action, 'id' => $key]);
                        } else {
                            return Yii::$app->getUrlManager()->createUrl([$action, 'id'=>$key]);
                        }
                    },
                ],
            ],
      //      'tableOptions' =>['class' => 'table table-striped table-bordered'],
        ]);
        ?>
    </div>
</div>
