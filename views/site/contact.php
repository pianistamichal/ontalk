<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = Yii::t('app', 'Contact');
?>
<!doctype html>
<div class="contact_page">
	<h3><?= Yii::t('app', 'Contact') ?></h3>
	<div class="container">
		<div class="col-md-8 cnt-pg">
			<?php
				if(Yii::$app->session->hasFlash('contactFormSubmitted')){?>
					<div class="alert alert-success">
						<?php echo Yii::t('app', 'Thank you for contacting us. We will try to respond as soon as possible') ?>
					</div>
				<?php }
			?>
			<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					'options' => ['class' => 'form-horizontal'],
					'fieldConfig' => [
						'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
						'labelOptions' => ['class' => 'col-md-4 control-label'],
					],
				]); ?>
			<?= $form->field($model, 'email')->textInput() ?>
			<?= $form->field($model, 'subject')->textInput() ?>
			<?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
			<?= $form->field($model, 'verifyCode')->widget(Captcha::className()) ?>
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-11">
					<?= Html::submitButton(Yii::t('app', 'Send'),['class'=>'btn btn-primary','name' => 'submit-button']);?>
				</div>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
		<div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/13.jpg');background-size:cover;">

		</div>
	</div>
</div>
<div class="map">
	<p>	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:300px;"><div id="gmap_canvas" style="height:300px;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.themecircle.net" id="get-map-data">themecircle.net</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(54.3716751,18.616327700000056),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(54.3716751, 18.616327700000056)});infowindow = new google.maps.InfoWindow({content:"<b>Gdansk Pomorskie</b><br/>Gabriela Narutowicza 11/12<br/> Politechnika Gdanska" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script><!--	<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d190028.76432096347!2d12.535997899999987!3d41.91007110000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f6196f9928ebb%3A0xb90f770693656e38!2sRome%2C+Italy!5e0!3m2!1sen!2sin!4v1436158065010" frameborder="0" style="border:0" allowfullscreen></iframe>-->
	</p>
</div>
<script>
	$(window).load(function(){
		var chatHeight = $('.content').height() - $('h3').outerHeight(true) - 3*2 - $('.map').outerHeight(true);
		$('.image-div').height(chatHeight);
		console.log(chatHeight);
	});
</script>

