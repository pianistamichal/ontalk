<script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Controller.js"></script>
<script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/View.js"></script>
<script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Database.js"></script>
<script src="<?= Yii::getAlias('@web') ?>/js/chat/refactor/Search.js"></script>
<script src="<?= Yii::getAlias('@web') ?>/js/jquery.chocolat.js"></script>
<script defer src="<?= Yii::getAlias('@web') ?>/js/jquery.flexslider.js"></script>
<script src="<?= Yii::getAlias('@web') ?>/js/jquery.wmuSlider.js"></script>
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
<script>
		var webPath = '<?= Yii::getAlias('@web') ?>';
		var controller = new Controller();

		$(window).unload(function(){
			$.ajax({
				type: 'POST',
				url: webPath + "/index.php/chat/end-searching",
				async: false
			});
		});
</script>
<div class="header-banner">
		<!-- Top Navigation -->
		<div class="container page-seperator">
		<section class="color bgi">

			<h1><?=Yii::t('app', 'Everybody starts out as strangers') ?></h1>

				<p><h4><?=Yii::t('app', 'Talk to people from any place you want. Press find button and meet some interesting people!') ?> <h4> </p>
			<button type="button" id="search-button"  data-loading-text=<?= Yii::t('app', 'Searching...')?> class="btn btn-primary btn-lg btn-search" autocomplete="off" ><?= Yii::t('app', 'Search')?></button>


		</section>

		<section class="col-3 ss-style-doublediagonal our-features">
				<h1><?=Yii::t('app', 'Our Features') ?></h1>
				<div class="column our-feat">
					<span class="mini_symbols" aria-hidden="true">
						<img src="<?= Yii::getAlias('@web') ?>/img/simple.png">
					</span>
					<h3><?=Yii::t('app', 'Minimalistic') ?></h3>
					<p>"<?=Yii::t('app', 'We Focus On Simplicity') ?>"<br><?=Yii::t('app', 'Just Things You Need') ?></p>
				</div>
				<div class="column our-feat">
					<span class="mini_symbols" aria-hidden="true">
						<img src="<?= Yii::getAlias('@web') ?>/img/modern.png">
					</span>
					<h3><?=Yii::t('app', 'Modern') ?></h3>
					<p>"<?=Yii::t('app', 'We Are "Up To Date"') ?>"<br> <?=Yii::t('app', 'We Care To Bring You New Technology Every Day') ?> </p>
				</div>
				<div class="column our-feat">
					<span class="mini_symbols" aria-hidden="true">
						<img src="<?= Yii::getAlias('@web') ?>/img/friendly.png">
					</span>
					<h3><?=Yii::t('app', 'Friendly') ?></h3>
					<p>"<?=Yii::t('app', 'Friendly mood') ?>"<br><?=Yii::t('app', 'We Are Here To Help You If You Have Any Problem') ?></p>
				</div>
				<div class="clearfix"></div>
		</section>
		</div>
</div>

<div class="our-work" id="our-work">
	<div class="container">
		<h2><?=Yii::t('app', 'On our service you can find:') ?></h2>

			<div class="gallery-bottom">
				<div class="col-md-4 bottom-gallery">
					<a href="<?= Yii::getAlias('@web') ?>/img/IMG_2.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img class="img-responsive" src="<?= Yii::getAlias('@web') ?>/img/IMG_2.jpg" />
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 ">
							<span class="txt"><?=Yii::t('app', 'Adventure') ?></span>
							<span class="glyphicon glyphicon-arrow-right gal-icn" aria-hidden="true"></span>
						</h3>
					</div>
					</a>
				</div>
				<div class="col-md-4 bottom-gallery">
					<a href="<?= Yii::getAlias('@web') ?>/img/IMG_1.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img class="img-responsive" src="<?= Yii::getAlias('@web') ?>/img/IMG_1.jpg" />
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 ">
							<span class="txt"><?=Yii::t('app', 'Fun') ?></span>
							<span class="glyphicon glyphicon-arrow-right gal-icn" aria-hidden="true"></span>
						</h3>
					</div>
					</a>
				</div>
				<div class="col-md-4 bottom-gallery">
					<a href="<?= Yii::getAlias('@web') ?>/img/IMG_5.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img class="img-responsive" src="<?= Yii::getAlias('@web') ?>/img/IMG_5.jpg" />
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 ">
							<span class="txt"><?=Yii::t('app', 'Contacts') ?></span>
							<span class="glyphicon glyphicon-arrow-right gal-icn" aria-hidden="true"></span>
						</h3>
					</div>
					</a>
				</div>
				<div class="col-md-4 bottom-gallery">
					<a href="<?= Yii::getAlias('@web') ?>/img/IMG_3.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img class="img-responsive" src="<?= Yii::getAlias('@web') ?>/img/IMG_3.jpg" />
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 ">
							<span class="txt"><?=Yii::t('app', 'Knowledge') ?></span>
							<span class="glyphicon glyphicon-arrow-right gal-icn" aria-hidden="true"></span>
						</h3>
					</div>
					</a>
				</div>
				<div class="col-md-4 bottom-gallery">
					<a href="<?= Yii::getAlias('@web') ?>/img/IMG_4.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img class="img-responsive" src="<?= Yii::getAlias('@web') ?>/img/IMG_4.jpg" />
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 ">
							<span class="txt"><?=Yii::t('app', 'Rest') ?></span>
							<span class="glyphicon glyphicon-arrow-right gal-icn" aria-hidden="true"></span>
						</h3>
					</div>
					</a>
				</div>
				<div class="col-md-4 bottom-gallery">
					<a href="<?= Yii::getAlias('@web') ?>/img/IMG_6.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
					<img class="img-responsive" src="<?= Yii::getAlias('@web') ?>/img/IMG_6.jpg" />
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 ">
							<span class="txt"><?=Yii::t('app', 'Peace') ?></span>
							<span class="glyphicon glyphicon-arrow-right gal-icn" aria-hidden="true"></span>
						</h3>
					</div>
					</a>
				</div>
				<div class="clearfix"></div>

				<!--light-box-files -->

				<script type="text/javascript">
					$(window).load(function(){
						$('.bottom-gallery a').Chocolat();
					});
				</script>
			</div>
			<!--<div class="testimonials">
				<h3>Testimonials</h3>
				<p>lorem ipsum simply a dummy text to format fonts and paragraphs</p>
				<div class="test-box">
					<q>The mind is everything. What you think you become</q>
						<a href="#" class="left">-Buddha</a>
					<div class="clearfix"></div>
					<span><img src="<?/*= Yii::getAlias('@web') */?>/img/half-arrow.png"></span>
				</div>
			</div>-->
	</div>
</div>

<section class="slider">
	<div class="container">
	<div class="flexslider">
		<ul class="slides">
			<li>
				<div class="banner-info">
				   <h3>Wielkie otwarcie strony</h3>
				   <h4><span class="m_2">"</span> To początek czegoś większego<span class="m_2"> "</span></h4>
				   <p> Jako twórcy OnTalk chcielibyśmy Was wszystkich powitać. Wierzymy, że strona służyć będzie dobru, którym niewątpliwie jest nieskrępowana rozmowa z nieznajomym przyjacielem. Dzięki nam możesz pisać o wszystkim, co daje Ci możliwość poznawania osób o podobnych poglądach, upodobaniach. Wszystko zależy od tego jak zaczniesz rozmowę z nieznajomym przyjacielem.
					   Jako, że strona dopiero powstała nastąpi na niej jeszcze wiele zmian, które poprawią wasz komfort podczas korzystania z serwisu.
					   <br>Zachęcamy do codziennych odwiedzin w poszukiwaniu ciekawych ludzi, a może w przyszłości [kto wie?] - przyjaciół :)
					   </br>
				   </p>
				  <!-- <h5><a href="#">Michał Błociński</a>, 10 września 2015</h5> -->
						<h5><b>Michał Błociński, 10 września 2015</b></h5>
				</div>
			</li>
			<li>
				<div class="banner-info">
				   <h3>Pomysły od użytkowników</h3>
				   <h4><span class="m_2">"</span>Zmieniamy się dla was<span class="m_2"> "</span></h4>
				   <p>Jako, że zależy nam na Twoim komforcie podczas korzystania z serwisu, prosimy o przesyłanie opinii i uwag pod zakładką
					   <a href="/index.php/site/contact">kontakt</a>.
					   <br>Naszą zaletą na tle konkurencji jest to, że cenimy sobie uwagi i pomysły naszych użytkowników.</br>
				   </p>
				  <!--  <h5><a href="#">Przemysław Petka</a>, 10 września 2015</h5>-->
					<h5><b>Przemysław Petka, 10 września 2015</b></h5>
				</div>
			</li>
			<!--<li>
				<div class="banner-info">
				   <h3>Our latest feeds</h3>
				   <h4><span class="m_2">"</span> lorem ipsum simply a dummy text to format fonts and paragraphs<span class="m_2"> "</span></h4>
				   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent.</p>
				   <h5><a href="#">Adam Chamler ,</a>Dateratr since 2014</h5>
				</div>
			</li>-->
		</ul>
	</div>
	</div>
	<!-- FlexSlider -->

		  <script type="text/javascript">
			$(window).load(function(){
			  $('.flexslider').flexslider({
				animation: "slide",
				start: function(slider){
				  $('body').removeClass('loading');
				}
			  });
			});
		  </script>
	<!-- FlexSlider -->
</section>
<!--<div class="subscribe-label">
	<div class="container">
		<div class="sub-cont">
			<div class="col-md-6">
			<h3>Subscribe to our newsletter</h3>
			</div>
			<div class="col-md-6">
			<form>
				<input class="mail" type="text" name="email" value="E mail" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E mail';}">
				<button type="button" class="btn btn-warning sub-btn">Notify</button>
			</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>-->



<div class="social-feeds">
<div class="container">
		<div class="wmuSlider example1 section" id="section-1">
		<article style="position: absolute; width: 100%; opacity: 0;">
		   <div class="bottom-slide">
				<img src="<?= Yii::getAlias('@web') ?>/img/brain.png" alt="" />
				<p><i><?=Yii::t('app', '"Words are singularly the most powerful force available to humanity"') ?></i><br>Yehuda Berg</br></p>
			</div>
		</article>
		 <article style="position: absolute; width: 100%; opacity: 0;">
			<div class="bottom-slide">
				<img src="<?= Yii::getAlias('@web') ?>/img/brain.png" alt="" />
				<p><i><?=Yii::t('app', '"Communication is a skill that you can learn. It\'s like riding a bicycle or typing. If you\'re willing to work at it, you can rapidly improve the quality of evry part of your life"') ?> </i> <br>Brian Tracy</br></p>
			</div>
		</article>
		 <article style="position: absolute; width: 100%; opacity: 0;">
			 <div class="bottom-slide">
				<img src="<?= Yii::getAlias('@web') ?>/img/brain.png" alt="" />
				<p><i><?=Yii::t('app', '"Good communication is just as stimulating as black coffee, and just as hard to sleep after"') ?></i> <br> Anne Morrow Lindbergh</br></p>
			</div>
		</article>
		<ul class="wmuSliderPagination">
			<li><a href="#" class="">0</a></li>
			<li><a href="#" class="">1</a></li>
			<li><a href="#" class="">2</a></li>
		</ul>
	</div>
	<!-- script -->

	<script>
	$('.example1').wmuSlider();
	</script>
	<!-- script -->
</div>
