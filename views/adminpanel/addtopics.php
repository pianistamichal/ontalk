<?php
use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'Add_Topic';
//$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::getAlias('@web') ?>/js/min-height-image.js"></script>
<div class="site-Add_Topics">
    <h3> <?= Yii::t('app', 'Add new #topic:') ?> </h3>
    <div class="container">
        <div class="col-md-8 cnt-pg">

            <?php $form = ActiveForm::begin([
                'id' => 'add_topics-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-4 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'topic')->textInput() ?>


            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-11">
                    <?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-primary', 'name' => 'new_topic-button']) ?>
                </div>
            </div>

            <?php
            if(Yii::$app->session->hasFlash('topic_added'))
                echo "<div class = 'alert alert-success'>" . Yii::$app->session->getFlash('topic_added')." </div>";
            ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/12.jpg');background-size:cover;">

        </div>
    </div>

</div>
