<?php
use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'Remove_Topic';
//$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::getAlias('@web') ?>/js/min-height-image.js"></script>

<div class="site-remove_topics">
    <h3> <?= Yii::t('app', 'Remove #topic:') ?> </h3>
    <div class="container">
        <div class="col-md-8 cnt-pg">
            <?php $form = ActiveForm::begin([
                'id' => 'remove_topics-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-4 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'topic')->textInput() ?>


            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-11">
                    <?= Html::submitButton(Yii::t('app', 'Remove'), ['class' => 'btn btn-primary', 'name' => 'remove_topic-button']) ?>
                </div>
            </div>

            <?php
            if(Yii::$app->session->hasFlash('topic_removed'))
                echo "<div class = 'alert alert-success'>" . Yii::$app->session->getFlash('topic_removed')." </div>";
            else if(Yii::$app->session->hasFlash('topic_not_removed'))
                echo "<div class = 'alert alert-danger'>" . Yii::$app->session->getFlash('topic_not_removed')." </div>";
            ?>


            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/12.jpg');background-size:cover;">

        </div>
    </div>
</div>
