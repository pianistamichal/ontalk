<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use  yii\data\ArrayDataProvider;
use app\models\Topics;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Messages');

?>
<head>
    <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web') ?>/css/contact.css">
</head>

<div class="site-messages" style="text-align: center">
    <h3><?= Html::encode($this->title) ?></h3>
        <div class="dataGridView">
            <?php
                 echo GridView::widget([
                     'dataProvider' => $provider,
                     'columns' => [
                         ['class' => 'yii\grid\SerialColumn',
                             'contentOptions'=>['style'=>'width: 30px;']],
                         //subject
                         [
                             'attribute' => 'subject',
                             'format' => 'text',
                             'label' => 'Subject',
                             'contentOptions'=>['style'=>'width: 10px;']
                         ],
                         //body
                         [
                             'attribute' => 'body',
                             'format' => 'text',
                             'label' => 'Body',
                             'contentOptions' => ['class' => 'text-wrap']
                         ],
                         [
                             'class' => 'yii\grid\ActionColumn',
                             'header'=>Yii::t('app', 'Action'),
                             'headerOptions' => ['width' => '70'],
                             'template' => '{deleterecord}{answerquestion}',
                             'buttons' => [
                                 'deleterecord' => function ($url, $model) {
                                     return Html::a('<span class="fa fa-search"></span>'. Yii::t('app', 'delete'), $url, [
                                         'class'=>'btn btn-primary btn-xs',
                                     ]);
                                 },
                                 'answerquestion' => function ($url, $model) {
                                     return Html::a('<span class="fa fa-search"></span>'. Yii::t('app', 'answer'), $url, [
                                         'class'=>'btn btn-primary btn-xs',
                                     ]);
                                 },
                             ],
                             'urlCreator' => function ($action, $model, $key, $index) {
                                 if ($action ==='deleterecord') {
                                     return Yii::$app->getUrlManager()->createUrl(['adminpanel/'.$action, 'id' => $key]);
                                 }
                                 else if($action === 'answerquestion'){
                                     return Yii::$app->getUrlManager()->createUrl(['adminpanel/'.$action, 'id' => $key]);
                                 }
                                 else {
                                     return Yii::$app->getUrlManager()->createUrl([$action, 'id' => $key]);
                                 }


                             },

                         ],
                     ],
                     'tableOptions' =>['class' => 'table table-striped table-bordered'],
                 ]);
            ?>
    </div>

</div>
