<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;


$this->title = Yii::t('app', 'Answerquestion');
?>
<!doctype html>
<div class="contact_page">
    <h3><?= Yii::t('app', 'Answer question') ?></h3>
    <?php
    if(Yii::$app->session->hasFlash("email_sent"))
        echo "<div class = 'alert alert-success'>" . Yii::$app->session->getFlash("email_sent")." </div>";
    ?>
    <div class="container">
        <div>
          <h4> Subject:</h4>
            <?= $info->subject;?>
        </div>
        <div>
          <h4>Message:</h4>
            <?= $info->body;?>
        </div>
        <div class="col-md-8 cnt-pg">

            <?php $form = ActiveForm::begin([
                'id' => 'answerquestion-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-4 control-label'],
                ],
            ]); ?>
            <?= $form->field($model, 'message_body')->textArea(['rows' => 6]) ?>

            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-11">
                    <?= Html::submitButton(Yii::t('app', 'Send'),['class'=>'btn btn-primary','name' => 'submit-button']);?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>


