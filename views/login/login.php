<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login') ;
//$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?= Yii::getAlias('@web') ?>/js/min-height-image.js"></script>
<div class="site-login">
    <h3><?=$this->title?></h3>

    <div class="container">
        <div class="col-md-8 cnt-pg">

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}<div class=\"col-md-4\">{input}</div>\n<div class=\"col-mg-4\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-4 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput() ?>
            <?= $form->field($model, 'password')->passwordInput() ?>


            <div class="form-group">
                <div class="col-lg-offset-4 col-lg-11">
                    <p><?= Yii::t('app', 'If you don\'t have an account') ?>
                        <a href="<?=\yii\helpers\Url::to(['/register/register'])?>"><?= Yii::t('app', 'register') ?></a></p>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
            <div>
                <p><h4><b> <?= Yii::t('app','Did you know?'); ?></b></h4></p>
                <p><h5><b> <?= Yii::t('app','Only loged users have acces to conversation history.'); ?></b></h5></p>
                <p><h5><b> <?= Yii::t('app','New alghoritm that search people depending on your personality soon!'); ?></b></h5></p>
            </div>
        </div>
        <div class="col-md-4 cnt-pg image-div" style="background: url('<?= Yii::getAlias('@web') ?>/img/12.jpg');background-size:cover;">

        </div>
    </div>

</div>