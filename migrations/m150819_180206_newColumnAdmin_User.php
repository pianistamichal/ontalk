<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_180206_newColumnAdmin_User extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE users ADD COLUMN user_role INT DEFAULT 0;");
    }

    public function down()
    {
        echo "m150819_180206_newColumnAdmin_User cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
