<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_150146_contact_update_AI extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE contact MODIFY ID INTEGER NOT NULL AUTO_INCREMENT;");
    }

    public function down()
    {
        echo "m150827_150146_contact_update_AI cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
