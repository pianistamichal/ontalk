<?php

use yii\db\Schema;
use yii\db\Migration;

class m150824_075247_newTopicTable extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `ontalk`.`topics` ( `ID` INT NOT NULL  AUTO_INCREMENT, `topic` TEXT NOT NULL , PRIMARY KEY (`ID`)) ENGINE = InnoDB");
    }

    public function down()
    {
        echo "m150824_075247_newTopicTable cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
