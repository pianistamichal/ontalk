<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_120649_conversation extends Migration
{
    public function up()
    {
        //TODO : refactor username1 to username
        $this->execute("CREATE TABLE `ontalk`.`conversation` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `username1` VARCHAR(30) NOT NULL,
  `text` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))");
    }


    public function down()
    {
        echo "m150826_120649_conversation cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
