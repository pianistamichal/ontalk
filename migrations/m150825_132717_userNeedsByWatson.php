<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_132717_userNeedsByWatson extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `ontalk`.`user_personalized` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `username` TEXT NOT NULL,
  `text` TEXT NULL DEFAULT NULL,
  `lastWatson` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`ID`))");
    }

    public function down()
    {
        echo "m150825_132717_userNeedsByWatson cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
