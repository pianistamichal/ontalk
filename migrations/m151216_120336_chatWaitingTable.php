<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_120336_chatWaitingTable extends Migration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE `ontalk`.`users_waiting` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `username` VARCHAR(45) NOT NULL,
              `chat_type_waiting` VARCHAR(120) NULL,
              PRIMARY KEY (`id`)
          )");
    }

    public function down()
    {
        echo "m151216_120336_chatWaitingTable cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
