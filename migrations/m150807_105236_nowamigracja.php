<?php

use yii\db\Schema;
use yii\db\Migration;

class m150807_105236_nowamigracja extends Migration
{
    /**
     *
     */
    public function up()
    {
        $this->execute('ALTER DATABASE `ontalk` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci');
        $this->execute("CREATE TABLE `ontalk`.`users` ( `ID` INT NOT NULL , `username` TEXT NOT NULL , `password` TEXT NOT NULL , `email` TEXT NOT NULL , PRIMARY KEY (`ID`)) ENGINE = InnoDB");
    }

    public function down()
    {
        echo "m150807_105236_nowamigracja cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
