<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_143148_contact_database_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `ontalk`.`contact` ( `ID` INT NOT NULL , `name` TEXT NOT NULL  , `email` TEXT NOT NULL ,`subject` TEXT NOT NULL , `body` TEXT NOT NULL , PRIMARY KEY (`ID`)) ENGINE = InnoDB");
    }

    public function down()
    {
        echo "m150827_143148_contact_database_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
