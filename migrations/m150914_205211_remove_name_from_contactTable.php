<?php

use yii\db\Schema;
use yii\db\Migration;

class m150914_205211_remove_name_from_contactTable extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE contact DROP COLUMN name;");
        
    }

    public function down()
    {
        echo "m150914_205211_remove_name_from_contactTable cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
