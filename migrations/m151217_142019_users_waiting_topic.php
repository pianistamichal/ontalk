<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_142019_users_waiting_topic extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE users_waiting ADD COLUMN room VARCHAR(32);");
        $this->execute("ALTER TABLE users_waiting ADD COLUMN topic TEXT;");
    }

    public function down()
    {
        echo "m151217_142019_users_waiting_topic cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
