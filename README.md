OnTalk Project
============================

Technology in project:
--------------
1.    Yii2
2.    Bootstrap
3.    Node.js socket server
4.    Primus, sockjs
5.    ajax, jQuery
6.    PHP, JS, CSS

How to start server. Fresh clone:
--------------
1.    On server like apache. Delete content in htdocs and make this folder as main folder of clone. I prefer to install xampp
2.    You must have mysql at least 5.0.0>
3.    On cmd execute this "git clone https://github.com/PianistaMichal/das7872aw7.git ."
4.    You can have this project in other folder in this way go into documentation of apache
5.    Into htdocs after clone. In cmd write yii migrate => yes(when it question You).
6.    Go into /web/primus/node_modules and run start_server.bat
7.    If You don't have already started apache service and mysql start it.
8.    Now You can go into web browser. Write localhost. Enter. Now You have website.
9.    It should work on default conf apache file.

Using server:
-------------
1.    User: 'Admin'; Pass: 'Admin1';
